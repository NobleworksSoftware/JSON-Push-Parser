package com.nobleworks_software.json.pushparser.internal

import com.nobleworks_software.json.pushparser.JsonParserException

class ValueParser internal constructor(parent: Parser) : BaseParser(parent)
{
    override fun parse(c: Int): Parser
            = when (c)
                {
                    in Int.MIN_VALUE .. ' '.toInt() -> this
                    't'.toInt() -> LiteralParser(this, "true", true)
                    'f'.toInt() -> LiteralParser(this, "false", false)
                    'n'.toInt() -> LiteralParser(this, "null", null)
                    in '0'.toInt() .. '9'.toInt() -> NumberParser(this, c)
                    '-'.toInt() -> NumberParser(this, c)
                    '\"'.toInt() -> StringParser(this)
                    '['.toInt() -> ArrayParser(this)
                    '{'.toInt() -> ObjectParser(this)

                    else -> throw JsonParserException("Expected a number, object, array, string, true, false, or null")
                }
}
