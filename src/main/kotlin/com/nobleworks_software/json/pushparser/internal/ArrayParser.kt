package com.nobleworks_software.json.pushparser.internal

import com.nobleworks_software.json.pushparser.JsonParserException

import java.util.ArrayList

class ArrayParser internal constructor(parent: Parser) : BaseParser(parent)
{
    private val array = ArrayList<Any?>()
    private val valueParser = ValueParser(this)
    private var needComma = false

    override fun parse(c: Int): Parser
            = when
            {
                c < 0 -> throw JsonParserException("Incomplete array definition")
                c <= ' '.toInt() -> this
                c == ']'.toInt() -> super.valueParsed(array)
                !needComma -> valueParser.parse(c)
                c == ','.toInt() -> valueParser.also { needComma = false }
                else -> throw JsonParserException("Expected , or ]")
            }

    override fun valueParsed(value: Any?)
            = this.also { array.add(value) }
                .also { needComma = true }
}
