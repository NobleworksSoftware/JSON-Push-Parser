package com.nobleworks_software.json.pushparser.internal

import com.nobleworks_software.json.pushparser.JsonParserException

/**
 * Created by dalek_000 on 2/24/2017.
 */
object ErrorStateParser : Parser
{
    override fun parse(c: Int): Parser
    {
        throw IllegalStateException("An Error occurred in the parser and the parser must be reset")
    }

    override fun valueParsed(o: Any?): Parser
    {
        throw IllegalStateException("An Error occurred in the parser and the parser must be reset")
    }
}
