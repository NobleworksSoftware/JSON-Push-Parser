package com.nobleworks_software.json.pushparser.internal

import com.nobleworks_software.json.pushparser.JsonParserException

internal class NumberParser(parent: Parser, c: Int) : BaseParser(parent)
{
    private val text = StringBuilder()
    private var needDigit = true
    private var leadingZero = false
    private var hasDecimal = false
    private var hasExponent = false
    private var hasExponentSign = false

    init
    {
        when (c)
        {
            '-'.toInt() -> text.appendCodePoint(c)
            else -> parse(c)
        }
    }

    private fun isDigit(c: Int) = c in '0'.toInt() .. '9'.toInt()

    override fun parse(c: Int): Parser
    {
        if (isDigit(c))
        {
            if (leadingZero)
            {
                throw JsonParserException("Leading zeroes not permitted in numbers")
            }
            else
            {
                text.appendCodePoint(c)

                if (needDigit && c == '0'.toInt() && !hasExponent && !hasDecimal)
                {
                    // Don't allow extraneous leading zeroes on the number
                    leadingZero = true
                }

                needDigit = false
            }
        }
        else if (needDigit)
        {
            if (c == '+'.toInt() || c == '-'.toInt())
            {
                if (hasExponent && !hasExponentSign)
                {
                    text.appendCodePoint(c)
                    hasExponentSign = true
                }
                else
                {
                    throw JsonParserException("Illegal use of sign in number")
                }
            }
            else
            {
                throw JsonParserException("Expected a digit")
            }
        }
        else if (c == '.'.toInt())
        {
            if (hasDecimal || hasExponent)
            {
                throw JsonParserException("Illegal use of decimal in number")
            }
            else
            {
                text.appendCodePoint(c)
                hasDecimal = true
                needDigit = true
                leadingZero = false
            }
        }
        else if (c == 'e'.toInt() || c == 'E'.toInt())
        {
            if (hasExponent)
            {
                throw JsonParserException("Repeated use of E in number")
            }
            else
            {
                text.appendCodePoint(c)
                hasExponent = true
                needDigit = true
                leadingZero = false
            }
        }
        else
        {
            return valueParsed(convertNumber()).parse(c)
        }

        return this
    }

    private fun convertNumber(): Any
            = if (hasDecimal || hasExponent)
            {
                text.toString().toDouble()
            }
            else
            {
                val l = text.toString().toLong()

                if (l in Integer.MIN_VALUE .. Integer.MAX_VALUE)
                {
                    l .toInt()
                }
                else
                {
                    l
                }
            }
}
