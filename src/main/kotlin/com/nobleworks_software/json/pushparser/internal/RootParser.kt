package com.nobleworks_software.json.pushparser.internal

import com.nobleworks_software.json.pushparser.JsonParserException

class RootParser(private val parserListener: (Any?) -> Unit) : Parser
{
    private val valueParser = ValueParser(this)

    override fun parse(c: Int): Parser
            = when (c)
                {
                    in Int.MIN_VALUE .. ' '.toInt() -> this
                    '['.toInt(), '{'.toInt() -> valueParser.parse(c)
                    else -> throw JsonParserException("Expected an object or array")
                }

    override fun valueParsed(o: Any?) = valueParser.also { parserListener(o) }
}
