package com.nobleworks_software.json.pushparser.internal

import com.nobleworks_software.json.pushparser.JsonParserException

class StringParser internal constructor(parent: Parser) : BaseParser(parent) {
    private val text = StringBuilder()

    private var unicodeEscapeValue = 0
    private var unicodeEscapeCounter = 0
    private var handlingEscape: Boolean = false

    override fun parse(c: Int): Parser
    {
        when
        {
            c < 0 -> throw JsonParserException("Unterminated string")
            c < ' '.toInt() -> throw JsonParserException("Control characters not allowed in string")
            handlingEscape ->
            {
                handlingEscape = false

                when (c) {
                    'u'.toInt() -> unicodeEscapeCounter = 4
                    'b'.toInt() -> text.appendCodePoint('\b'.toInt())
                    't'.toInt() -> text.appendCodePoint('\t'.toInt())
                    'f'.toInt() -> text.appendCodePoint(12)
                    'n'.toInt() -> text.appendCodePoint('\n'.toInt())
                    'r'.toInt() -> text.appendCodePoint('\r'.toInt())
                    else -> text.appendCodePoint(c)
                }
            }
            unicodeEscapeCounter > 0 ->
            {
                unicodeEscapeValue *= 16

                unicodeEscapeValue += c - when(c)
                {
                    in '0'.toInt() .. '9'.toInt() -> '0'.toInt()
                    in 'A'.toInt() .. 'F'.toInt() -> 'A'.toInt() - 10
                    in 'a'.toInt() .. 'f'.toInt() -> 'a'.toInt() - 10
                    else -> throw JsonParserException("Expected hex digit for unicode escape")
                }

                if (--unicodeEscapeCounter == 0)
                {
                    text.appendCodePoint(unicodeEscapeValue)
                }
            }
            c == '\"'.toInt() -> return valueParsed(text.toString())
            c == '\\'.toInt() -> handlingEscape = true
            else -> text.appendCodePoint(c)
        }

        return this
    }
}
