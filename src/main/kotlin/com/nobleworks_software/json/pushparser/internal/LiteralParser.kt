package com.nobleworks_software.json.pushparser.internal

import com.nobleworks_software.json.pushparser.JsonParserException

import java.text.CharacterIterator
import java.text.StringCharacterIterator

class LiteralParser(parent: Parser, text: String, private val value: Any?) : BaseParser(parent)
{
    private val expected: CharacterIterator = StringCharacterIterator(text, 1)

    override fun parse(c: Int)
            = when
            {
                c != expected.current().toInt() -> throw JsonParserException("Expected " + expected.toString())
                expected.next() == CharacterIterator.DONE -> valueParsed(value)
                else -> this
            }
}
