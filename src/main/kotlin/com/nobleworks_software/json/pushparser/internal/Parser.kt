package com.nobleworks_software.json.pushparser.internal

import com.nobleworks_software.json.pushparser.JsonParserException

interface Parser
{
    @Throws(JsonParserException::class)
    fun parse(c: Int): Parser

    fun valueParsed(o: Any?): Parser
}
