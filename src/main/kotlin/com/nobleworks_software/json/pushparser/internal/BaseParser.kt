package com.nobleworks_software.json.pushparser.internal

abstract class BaseParser(private val parent: Parser) : Parser
{
    override fun valueParsed(value: Any?) = parent.valueParsed(value)
}
