package com.nobleworks_software.json.pushparser.internal

import com.nobleworks_software.json.pushparser.JsonParserException

import java.util.HashMap

internal class ObjectParser(parent: Parser) : BaseParser(parent)
{
    private val values = HashMap<String, Any?>()

    private val nameParser = StringParser(this)
    private val valueParser = ValueParser(this)

    private var name: String? = null
    private var needComma: Boolean = false
    private var needColon: Boolean = false

    private fun Int.requireCharacter(requiredCharacter: Char, error: String)
        = if(this == requiredCharacter.toInt()) this@ObjectParser else throw JsonParserException(error)

    override fun parse(c: Int)
            = when
            {
                c < 0 -> throw JsonParserException("Incomplete object definition")
                c <= ' '.toInt() -> this
                needColon -> c.requireCharacter(':', "Expected ':' after property name")
                        .also { needColon = false }
                name != null -> valueParser.parse(c)
                c == ']'.toInt() -> super.valueParsed(values)
                needComma -> c.requireCharacter(',', "Expected , or }")
                    .also { needComma = false }

                c == '\"'.toInt() -> nameParser
                else -> throw JsonParserException("Expected '\"' or ]")
            }

    override fun valueParsed(value: Any?): Parser
    {
        if (name == null)
        {
            name = value as String
            needColon = true
        }
        else
        {
            values.put(name!!, value)
            name = null
            needComma = true
        }

        return this
    }
}
