package com.nobleworks_software.json.pushparser

class JsonPushByteParser(listener: (Any?) -> Unit)
{
    companion object
    {
        private const val Utf8 = 1
        private const val Utf16BE = 2
        private const val Utf32BE = 4
        private const val Utf16LE = 8
        private const val Utf32LE = 16

        private const val unknownEncoding = Utf8 or Utf16BE or Utf16LE or Utf32BE or Utf32LE
    }

    private val parser: JsonPushCodePointParser = JsonPushCodePointParser(listener)

    private var character = 0
    private var characterCount = 0

    private var possibleEncodings = unknownEncoding

    internal fun reset()
    {
        characterCount = 0
        possibleEncodings = unknownEncoding

        parser.reset()
    }

    private fun error(message: String) : Nothing
    {
        possibleEncodings = 0

        throw IllegalArgumentException(message)
    }

    internal fun parse(c: Int)
    {
        when (possibleEncodings)
        {
            unknownEncoding ->
                possibleEncodings = when(c)
                {
                    0 -> (Utf16BE or Utf32BE).also { handleUtf16And32(c) }
                    in 1..127 -> (Utf8 or Utf16LE or Utf32LE).also { parser.parse(c) }
                    else -> error("Invalid initial byte, must be ASCII or NULL")
                }

            Utf16BE or Utf32BE ->
            {
                possibleEncodings = if (c == 0) Utf32BE else Utf16BE

                handleUtf16And32(c)
            }

            Utf8 or Utf16LE or Utf32LE -> detectUtf8OrUtf16OrUtf32LittleEndian(c)

            Utf8 -> handleUtf8(c)

            Utf16LE, Utf16BE, Utf32LE, Utf32BE -> handleUtf16And32(c)

            else -> error("Previous input to character decoder was invalid, reset to continue")
        }
    }

    private fun detectUtf8OrUtf16OrUtf32LittleEndian(c: Int)
    {
        // After an initial ASCII character count the number of
        // null bytes that follow it
        if (c == 0 && characterCount < 3)
        {
            characterCount++
        }
        else
        {
            // How many null bytes followed the initial ASCII character
            // The only valid answers are 0, 1, or 3 which determines
            // the character encoding
            possibleEncodings = when (characterCount)
            {
                0 -> Utf8
                1 -> Utf16LE
                3 -> Utf32LE
                else -> error("Invalid initial character, ASCII followed by only 2 NULLS")
            }

            // Now that we have determined the encoding and moved past any nulls
            // for the upper bytes of UTF-16LE or UTF-32BE start decoding the
            // second character
            characterCount = 0

            parse(c)
        }
    }

    private fun handleUtf8(c: Int)
    {
        if (characterCount == 0)
        {
            when
            {
                c < 128 -> parser.parse(c)
                c and 0xE0 == 0xC0 ->
                {
                    character = c and 0x1F
                    characterCount = 1
                }
                c and 0xF0 == 0xE0 ->
                {
                    character = c and 0xF
                    characterCount = 2
                }
                c and 0xF8 == 0xF0 ->
                {
                    character = c and 0x7
                    characterCount = 3
                }
                else -> error("Illegal first byte for utf-8")
            }
        }
        else if (c > 0 && c and 0xC0 == 0x80)
        {
            character = (character shl 6) or (c and 0x3F)

            characterCount--

            if (characterCount == 0)
            {
                parser.parse(character)
            }
        }
        else
        {
            error("Expected continuation character for utf-8")
        }
    }

    private fun handleUtf16And32(c: Int)
    {
        if (c < 0)
        {
            when(characterCount)
            {
                // If it was an EOF pass that on
                0 -> parser.parse(c)
                else -> error("Encountered EOF while decoding a multi-byte character")
            }
        }
        else
        {
            if (characterCount == 0)
            {
                character = 0
            }

            character = (c and 0xFF).let()
            {
                when(possibleEncodings)
                {
                    Utf32BE, Utf16BE -> (character shl 8) or it
                    else -> character or (c shl 8 * characterCount)
                }
            }

            characterCount++

            characterCount %= when(possibleEncodings)
            {
                Utf16BE, Utf16LE -> 2
                else -> 4
            }

            if (characterCount == 0)
            {
                parser.parse(character)
            }
        }
    }
}
