package com.nobleworks_software.json.pushparser


import com.nobleworks_software.json.pushparser.internal.ErrorStateParser
import com.nobleworks_software.json.pushparser.internal.Parser
import com.nobleworks_software.json.pushparser.internal.RootParser

class JsonPushCodePointParser(val parserListener: (Any?) -> Unit)
{
    private lateinit var parser: Parser

    init { reset() }

    fun reset()
    {
        parser = RootParser(parserListener)
    }

    fun parse(c: Int)
    {
        try
        {
            parser = parser.parse(c)
        }
        catch(e: Exception)
        {
            parser = ErrorStateParser

            throw e
        }
    }
}
