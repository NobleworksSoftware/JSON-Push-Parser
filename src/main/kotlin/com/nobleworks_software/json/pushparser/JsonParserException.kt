package com.nobleworks_software.json.pushparser

@SuppressWarnings("serial")
class JsonParserException(message: String) : Exception(message)